DEPLOY_DIR=tom8080
APP_GROUP=com.real.common
APP=accountmgmt
APP_C=war

CONFIG_GROUP=com.rhapsody.platform.accountmgmt
CONFIG=accountmgmt-config
CONFIG_C=$2

SERVICE=rhapsodydirectaccountmgmt

WAR_FILE=${SERVICE}.war

if [ $# -eq 0 ] 
    then  echo "Usage accountmgmt.sh releases|snapshots env{test,int,beta,prod}"  
    exit 1
fi

if [ -d "tom8080" ]
   then echo "rm tom8080";rm -Rf ./tom8080/*
fi

VERSION=LATEST

if [ $# -eq 3 ]
   then
   echo "not latest version: $3"
   VERSION=$3
fi

echo "Service Name $WAR_FILE"

echo ../ecomm_download.sh $1  $APP_GROUP $APP war $VERSION $WAR_FILE $APP_C

../ecomm_download.sh $1  $APP_GROUP $APP war $VERSION $WAR_FILE $APP_C
mkdir -p $DEPLOY_DIR/webapps && unzip  ${SERVICE}.war -d $SERVICE  && mv ${SERVICE} $DEPLOY_DIR/webapps && rm  $SERVICE.war

mkdir $DEPLOY_DIR/conf/ && cp ./conf/* $DEPLOY_DIR/conf/ && chmod 700  $DEPLOY_DIR/conf/jmxremote.*


../ecomm_download.sh $1 $CONFIG_GROUP  $CONFIG  tar LATEST $CONFIG_C-config.tar $2
mkdir -p $DEPLOY_DIR/conf && tar xvf  $CONFIG_C-config.tar -C $DEPLOY_DIR/conf
chmod 700 $DEPLOY_DIR/conf/*
rm  $CONFIG_C-config.tar
