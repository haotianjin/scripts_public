REPO_URL=http://rds-repo-dev-1201.sea2.rhapsody.com:8080/nexus
REPO=$1
GROUP=$2
ARTIFACT=$3
EXT=$4
VERSION=$5
O=$6
CLASS=$7

echo "curl  -J -L  $REPO_URL/service/local/artifact/maven/content?r=$1&g=$2&a=$3&e=$4&v=$5&c=$7 -o $6"
curl  -J -L  "$REPO_URL/service/local/artifact/maven/content?r=$1&g=$2&a=$3&e=$4&v=$5&c=$7" -o $6
