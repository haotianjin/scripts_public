DEPLOY_DIR=tom8080
APP_GROUP=com.rhapsody.ecomm
APP=ups

CONFIG_GROUP=com.rhapsody.ecomm
CONFIG=ups-config
CONFIG_C=$2

SERVICE=ups

if [ $# -eq 0 ] 
    then  echo "Usage ups.sh releases|snapshots env{test,int,beta,prod}"  
    exit 1
fi

if [ -d "tom8080" ]
   then echo "rm tom8080";rm -Rf ./tom8080/*
fi

VERSION=LATEST

if [ -n "$3" ]
   then VERSION=$3
fi

rm -Rf tom8080
../ecomm_download.sh $1  $APP_GROUP $APP war $VERSION ${SERVICE}.war 
mkdir -p $DEPLOY_DIR/webapps && unzip  ${SERVICE}.war -d ${SERVICE}  && mv ${SERVICE} $DEPLOY_DIR/webapps && rm  ${SERVICE}.war
../ecomm_download.sh releases $CONFIG_GROUP  $CONFIG  tar LATEST $CONFIG_C-config.tar $CONFIG_C
mkdir -p $DEPLOY_DIR/conf/$SERVICE && tar xvf  $CONFIG_C-config.tar -C $DEPLOY_DIR/conf/$SERVICE
chmod 700 $DEPLOY_DIR/conf/$SERVICE/*
rm  $CONFIG_C-config.tar
cp /opt/operations/$2/ups/operations.properties $DEPLOY_DIR/conf/$SERVICE;chmod 700 $DEPLOY_DIR/conf/$SERVICE

