if [ $# -eq 0 ]
    then  echo "Usage build-rds.sh env{test,int,beta,prod}"
    exit 1
fi


cd ./accountmgmt &&  docker build -t "platform/accountmgmt:latest-$1" . ; cd ..
cd ./library &&   docker build -t "platform/library:latest-$1" . ; cd ..
cd ./playlist  && docker build -t "platform/playlist:latest-$1" . ; cd ..

