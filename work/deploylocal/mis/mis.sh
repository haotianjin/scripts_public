DEPLOY_DIR=tom8080
APP_GROUP=com.rhapsody.ecomm
APP=mis
APP_C=war

CONFIG_GROUP=com.rhapsody.ecomm
CONFIG=mis-config
CONFIG_C=$2

SERVICE=mis

if [ $# -eq 0 ] 
    then  echo "Usage playlist.sh releases|snapshots env{test,int,beta,prod}"  
    exit 1
fi

if [ -d "tom8080" ]
   then echo "rm tom8080";rm -Rf ./tom8080/*
fi

VERSION=LATEST

if [ $# -eq 3 ]
   then
   echo "not latest version: $3"
   VERSION=$3
fi




../ecomm_download.sh $1  $APP_GROUP $APP war $VERSION  ${SERVICE}.war
mkdir -p $DEPLOY_DIR/webapps && unzip  ${SERVICE}.war -d ${SERVICE}  && mv ${SERVICE} $DEPLOY_DIR/webapps && rm  ${SERVICE}.war

../ecomm_download.sh $1 $CONFIG_GROUP  $CONFIG  tar LATEST $CONFIG_C-config.tar $2
mkdir -p $DEPLOY_DIR/conf/mis && tar xvf  $CONFIG_C-config.tar -C $DEPLOY_DIR/conf/mis

#cp /opt/operations/$2/mis/operation.properties $DEPLOY_DIR/conf/mis/

#chmod 600  $DEPLOY_DIR/conf/mis/operation.properties

mkdir -p $DEPLOY_DIR/conf
rm  $CONFIG_C-config.tar

#cp /opt/operations/$2/accountmgmt/*  $DEPLOY_DIR/conf/
cp ./conf/* $DEPLOY_DIR/conf/
chmod 700 $DEPLOY_DIR/conf/*
#chmod 700  $DEPLOY_DIR/conf/jmxremote.*

