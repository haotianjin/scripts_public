DEPLOY_DIR=tom8080
APP_GROUP=com.real.common
APP=playback
APP_C=war

CONFIG_GROUP=com.real.common
CONFIG=configs
CONFIG_C=playback

SERVICE=rhapsodydirectplayback

if [ $# -eq 0 ] 
    then  echo "Usage playback.sh public|snapshots env{test,int,beta,prod}"  
    exit 1
fi

if [ -d "tom8080" ]
   then echo "rm tom8080";rm -Rf ./tom8080/*
fi


VERSION=LATEST

if [ -n "$3" ]
   then VERSION=$3
fi

../rds_download.sh $1  $APP_GROUP $APP war $VERSION  ${SERVICE}.war $APP_C
#mkdir -p $DEPLOY_DIR/webapps && unzip  ${SERVICE}.war -d ${SERVICE}  && mv ${SERVICE} $DEPLOY_DIR/webapps && rm  ${SERVICE}.war
mkdir -p $DEPLOY_DIR/webapps &&  mv  ${SERVICE}.war  $DEPLOY_DIR/webapps/
../rds_download.sh $1 $CONFIG_GROUP  $CONFIG  tar $VERSION $CONFIG_C-config.tar $CONFIG_C-$2
mkdir -p $DEPLOY_DIR/conf && tar xvf  $CONFIG_C-config.tar -C $DEPLOY_DIR/conf
chmod 700 $DEPLOY_DIR/conf/*
rm  $CONFIG_C-config.tar

