DEPLOY_DIR=tom8080
APP_GROUP=com.real.ecs
APP=pws

CONFIG_GROUP=com.real.ecs
CONFIG=pws-config
CONFIG_C=$2

SERVICE=pws

if [ $# -eq 0 ] 
    then  echo "Usage pws.sh releases|snapshots env{test,int,beta,prod}"  
    exit 1
fi

if [ -d "tom8080" ]
   then echo "rm tom8080";rm -Rf ./tom8080/* 
fi

../ecomm_download.sh $1  $APP_GROUP $APP war LATEST ${SERVICE}.war 
mkdir -p $DEPLOY_DIR/webapps && unzip  ${SERVICE}.war -d ${SERVICE}  && mv ${SERVICE} $DEPLOY_DIR/webapps && rm  ${SERVICE}.war
../ecomm_download.sh releases $CONFIG_GROUP  $CONFIG  tar LATEST $CONFIG_C-config.tar $CONFIG_C
mkdir -p $DEPLOY_DIR/app/config/$SERVICE && tar xvf  $CONFIG_C-config.tar -C $DEPLOY_DIR/app/config/$SERVICE
chmod 700 $DEPLOY_DIR/app/config/$SERVICE/*
rm  $CONFIG_C-config.tar
cp /opt/operations/$2/$SERVICE/* $DEPLOY_DIR/app/config/$SERVICE/ && chmod 600 $DEPLOY_DIR/app/config/$SERVICE/*

