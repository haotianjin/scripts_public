if [ $# -eq 0 ]
    then  echo "Usage download-rds.sh env{test,int,beta,prod}"
    exit 1
fi

echo "Download Accountmgmt"
cd ./accountmgmt && ./accountmgmt.sh releases $1 && cd ..
echo "Download Library"
cd ./library &&  ./library.sh snapshots $1 && cd ..
echo "Download playlist"
cd ./playlist && ./playlist.sh releases $1 && cd ..

