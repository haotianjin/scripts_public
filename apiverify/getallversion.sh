#/bin/sh
export env=prod

./pingversion ecws-batch-prod-1201.sea2.rhapsody.com 8080 ecws

for app in ecws rcws
do
./get${app}version.sh $app
done

app=rpiws
for i in 01 02 03 04 
do
./pingversion ${app}-${env}-12$i.sea2.rhapsody.com 8080 $app
done

app=csgw
for i in 01 02
do
./pingversion cs-${env}-12$i.sea2.rhapsody.com 8080 $app
done
